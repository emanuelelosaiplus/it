# RKE2 HA CONFIG #

## 1 - Installing Kubernetes ##

### **Create config.yaml file** ###

The config.yaml file has to be placed at /etc/rancher/rke2/config.yaml 

#### **For the first node, it has to look like this:** ####

token: my-shared-secret  

#### **For the other nodes:** ####
 
server: https://address:9345  
token: my-shared-secret 

### **Install, Enable and Start RKE2 the nodes** ###
1. curl -sfL https://get.rke2.io | sh -
2. systemctl enable rke2-server.service
3. systemctl start rke2-server.service


## 2 - Confirm that RKE2 is running ##

### **Add kubectl to PATH** ###

* export PATH=/var/lib/rancher/rke2/bin/:$PATH

### **Ensure that the cluster has come up** ###

* kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml get nodes

### **Test the health of the cluster pods** ###

* kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml get pods --all-namespaces


## 3 - Save and start using the kubeconfig file ##

### **Copy the kubeconfig file** ###
1. mkdir ~/.kube/config
2. cp /etc/rancher/rke2/rke2.yaml ~/.kube/config

### **Specify the kubeconfig file you want to use** ###
* kubectl --kubeconfig ~/.kube/config/rke2.yaml get pods --all-namespaces


## 4 - Check the health of the cluster pods ##
* kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml get pods -A



