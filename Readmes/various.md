kubectl describe pod {podname}

kubectl exec {podname} -it -- bash

kubectl logs {podname}

Se non viene usato subPath: {keyName}, il mountpoint diventa una directory dove vengono messe tutte le chiavi della config map.
Per far si che il file specificato contenga le proprietà usate dalle chiavi usare subPath.